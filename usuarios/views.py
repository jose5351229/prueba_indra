from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, TemplateView, ListView, UpdateView, DeleteView
from usuarios.forms import UsuarioCreateForm
from usuarios.models import Usuario


class indexView(LoginRequiredMixin, TemplateView):
    template_name = 'index.html'
    
    def get_context_data(self, **kwargs):
        contex = super(indexView, self).get_context_data()
        contex['usuario'] = Usuario.objects.filter(id=self.request.user.id).last()
        return contex


class UsuarioListView(LoginRequiredMixin, ListView):
    model = Usuario
    template_name = 'list.html'

    def get_context_data(self, **kwargs):
        contex = super(UsuarioListView, self).get_context_data()
        contex['usuario'] = Usuario.objects.filter(id=self.request.user.id).last()
        return contex


class UsuarioCreateView(LoginRequiredMixin, CreateView):
    model = Usuario
    form_class = UsuarioCreateForm
    template_name = 'usuarios/usuario_form.html'
    success_url = reverse_lazy('listar_usuario')

    def get_context_data(self, **kwargs):
        contex = super(UsuarioCreateView, self).get_context_data()
        contex['usuario'] = Usuario.objects.filter(id=self.request.user.id).last()
        return contex

    def form_valid(self, form):
        usuario = form.save(commit=False)
        usuario.set_password(usuario.password)
        return super(UsuarioCreateView, self).form_valid(form)


class UsuarioUpdateView(LoginRequiredMixin, UpdateView):
    model = Usuario
    form_class = UsuarioCreateForm
    template_name = 'usuarios/usuario_form.html'
    success_url = reverse_lazy('listar_usuario')

    def get_context_data(self, **kwargs):
        contex = super(UsuarioUpdateView, self).get_context_data()
        contex['usuario'] = Usuario.objects.filter(id=self.request.user.id).last()
        return contex

    def form_valid(self, form):
        usuario = form.save(commit=False)
        usuario.set_password(usuario.password)
        return super(UsuarioUpdateView, self).form_valid(form)


class UsuarioDeleteView(LoginRequiredMixin, DeleteView):
    success_url = reverse_lazy('listar_usuario')
    model = Usuario
    template_name = '_confirm_delete.html'

    def get_context_data(self, **kwargs):
        contex = super(UsuarioDeleteView, self).get_context_data()
        contex['usuario'] = Usuario.objects.filter(id=self.request.user.id).last()
        return contex
