from django.db import models
from django.contrib.auth.models import User
from django.utils.encoding import smart_str

# Create your models here.


class Usuario(User):
    def generate_filename(self, filename):
        path = "usuarios/%s/%s" % (self.username, smart_str(filename))
        return path
    archivo = models.ImageField(upload_to=generate_filename, blank=True, null=True)
