from django.urls import path
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    path('inicio/', views.indexView.as_view(), name='inicio'),
    path('', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('crear-usuario/', views.UsuarioCreateView.as_view(), name='crear_usuario'),
    path('listar-usuario/', views.UsuarioListView.as_view(), name='listar_usuario'),
    path('editar-usuario/<int:pk>', views.UsuarioUpdateView.as_view(), name='editar_usuario'),
    path('eliminar-usuario/<int:pk>', views.UsuarioDeleteView.as_view(), name='eliminar_usuario'),
]