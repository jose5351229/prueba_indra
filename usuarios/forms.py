from django import forms
from secrets import compare_digest
from usuarios.models import Usuario


class UsuarioCreateForm(forms.ModelForm):
    required_css_class = 'required'
    password2 = forms.CharField(label='Confirmar contraseña',
                                required=True,
                                widget=forms.PasswordInput(attrs={'class': 'form-control',
                                                                  'placeholder': 'Ingrese de nuevo su contraseña'}))

    def __init__(self, *args, **kwargs):
        super(UsuarioCreateForm, self).__init__(*args, **kwargs)
        self.fields['password'].widget = forms.PasswordInput()
        self.fields['is_staff'].label = 'Usuario Admin'
        self.fields['email'].required = True
        self.fields['email'].label = 'Correo electrónico'

    class Meta:
        model = Usuario
        fields = ['username', 'first_name', 'last_name', 'password', 'password2', 'email', 'is_staff', 'archivo']
        help_texts = {
            'foto': 'Solo se admiten formatos xls, xlsx, pdf, doc, docx, ppt, pps, png, jpg, txt, ods, odt.',
        }

    def clean(self):
        password = self.cleaned_data['password']
        password2 = self.cleaned_data['password2']
        if not compare_digest(password, password2):
            self.add_error('password2', 'Las contraseñas no son iguales')
        username = self.cleaned_data['username']
        usuario = Usuario.objects.filter(username=username).exclude(id=self.instance.pk).first()
        if usuario:
            self.add_error('username', 'Este nombre de usuario no se encuentra disponible')

        if len(password) < 5:
            self.add_error('password', 'La contraseña debe tener al menos cinco caracteres. ')
        return self.cleaned_data
