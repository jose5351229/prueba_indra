# prueba_indra
# INSTALACIÓN DEL PROYECTO EN UBUNTU.
# 1. CLONAR REPOSITORIO.
    git clone https://gitlab.com/jose5351229/prueba_indra.git
# 2. INSTALAR PYTHON3.8 AND VIRTUALENV.
    sudo apt install python3.8
    pip3.8 install virtualenv
# 3. CREAR AMBIENTE.
    virtualenv <env-name> -p python3.8
    source <env-path>/bin/activate
# 4. INSTALAR REQUIREMENTS.
    pip install -r requirements.txt
# 6. SE CORRE EL SERVICIO DE DJANGO.
    python manage.py runserver 8000
    
# 7. USUARIOS
    Usuario1:
    username: admin
    password: admin
    tipo: admin

    Usuario2:
    username: jose535
    password: 123456
    tipo: no admin